package com.example.telegaTiny.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "messages")
public class MessageEntity {
    @Id
    private String uuid;

    private String subject;
    @Column(name = "message_text")
    private String messageText;
}
