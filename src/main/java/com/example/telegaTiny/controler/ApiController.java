package com.example.telegaTiny.controler;

import com.example.telegaTiny.data.Message;
import com.example.telegaTiny.data.Messages;
import com.example.telegaTiny.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api")
public class ApiController {

    @Autowired
    private MessageService messageService;

    @GetMapping("messages")
    public ResponseEntity<Messages> getMessages() {
        return new ResponseEntity<>(messageService.getMessages(), HttpStatus.OK);
    }

    @PostMapping("messages")
    public ResponseEntity<Message> createMessage(@RequestBody Message message) {
        return new ResponseEntity<>(messageService.createMessage(message), HttpStatus.OK);
    }
}
