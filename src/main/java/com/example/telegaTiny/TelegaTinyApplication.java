package com.example.telegaTiny;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TelegaTinyApplication {

	public static void main(String[] args) {
		SpringApplication.run(TelegaTinyApplication.class, args);
	}

}
