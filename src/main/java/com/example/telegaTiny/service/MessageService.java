package com.example.telegaTiny.service;

import com.example.telegaTiny.data.Message;
import com.example.telegaTiny.data.Messages;
import org.springframework.stereotype.Service;


public interface MessageService {
    Messages getMessages();

    Message createMessage(Message message);
}
