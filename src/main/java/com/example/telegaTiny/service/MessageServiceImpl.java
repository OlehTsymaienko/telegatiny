package com.example.telegaTiny.service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.example.telegaTiny.dao.MessageEntity;
import com.example.telegaTiny.dao.MessageRepository;
import com.example.telegaTiny.data.Message;
import com.example.telegaTiny.data.Messages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MessageServiceImpl implements MessageService {
    @Autowired
    private MessageRepository messageRepository;

    @Override
    public Messages getMessages() {
        List<MessageEntity> all = messageRepository.findAll();
        List<Message> messages = new ArrayList<>();
        for (MessageEntity entity : all) {
            messages.add(convert(entity));
        }
        return createMessages(messages);
    }

    private Messages createMessages(List<Message> messages) {
        Messages res = new Messages();
        res.setMessageList(messages);
        return res;
    }

    @Override
    public Message createMessage(Message message) {
        MessageEntity save = messageRepository.save(convert(message));
        return convert(save);
    }

    private MessageEntity convert(Message message) {
        MessageEntity entity = new MessageEntity();
        entity.setUuid(UUID.randomUUID().toString());
        entity.setMessageText(message.getText());
        entity.setSubject(message.getSubject());
        return entity;
    }

    private Message convert(MessageEntity entity) {
        Message message = new Message();
        message.setUuid(entity.getUuid());
        message.setSubject(entity.getSubject());
        message.setText(entity.getMessageText());
        return message;
    }
}
