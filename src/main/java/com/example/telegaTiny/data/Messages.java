package com.example.telegaTiny.data;

import java.util.List;

import lombok.Data;

@Data
public class Messages {
    private List<Message> messageList;
}
