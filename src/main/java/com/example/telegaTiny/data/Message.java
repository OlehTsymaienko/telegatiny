package com.example.telegaTiny.data;

import lombok.Data;

@Data
public class Message {
    private String uuid;
    private String subject;
    private String text;
}
